package GameState;

import java.awt.*;
import java.awt.event.KeyEvent;

import Entity.*;
import Main.GamePanel;
import TileMap.Background;
import TileMap.TileMap;



public class Level1State extends GameState {
	
	private TileMap tileMap;
	private Background bg;
	private Player player;
	private int count;
	String numberAsString;
	String retry = "Retries: ";
	
	public Level1State(GameStateManager gsm) {
		this.gsm = gsm;
		init();

	}

	public void init() {

		tileMap = new TileMap(30);
		tileMap.loadTiles("/Tilesets/grasstileset.gif");
		tileMap.loadMap("/Maps/bloxs.map");
		tileMap.setPosition(0, 0);
		tileMap.setTween(0.07);		
		
		bg = new Background("/Backgrounds/forest_bg.jpg", 1);
		
		player = new Player(tileMap);
		player.setPosition(120, 350);
	}

	public void update() {
		
		player.update();
		
		tileMap.setPosition(GamePanel.WIDTH / 2 - player.getx(),
				GamePanel.HEIGHT / 2 - player.gety()
				);
	}

	public void draw(Graphics2D g) {

		bg.draw(g);

		// draw Tilemap
		tileMap.draw(g);

		// draw player
		player.draw(g);
		
		g.setColor(Color.RED);
		g.drawString( retry, 0, 25);
		g.drawString(String.valueOf(count), 90, 25);
		g.drawString("Level 1" , 480, 25);

	}

	public void keyPressed(int k) {
		if(k== KeyEvent.VK_LEFT) player.setLeft(true);
		if(k== KeyEvent.VK_RIGHT) player.setRight(true);
		if(k== KeyEvent.VK_UP) player.setUp(true);
		if(k== KeyEvent.VK_DOWN) player.setDown(true);
		if(k== KeyEvent.VK_LEFT) player.setLeft(true);
		if(k== KeyEvent.VK_SPACE) player.setJumping(true);
		if(k== KeyEvent.VK_SHIFT) player.setBuff(true);
		if(k== KeyEvent.VK_CONTROL) player.setDebuff(true);
		
		if(k== KeyEvent.VK_R) {
			{
			player.setPosition(120, 350);
			count = count + 1;
			}
			
		}
				
	}
		
	public void keyReleased(int k) {
		if(k== KeyEvent.VK_LEFT) player.setLeft(false);
		if(k== KeyEvent.VK_RIGHT) player.setRight(false);
		if(k== KeyEvent.VK_UP) player.setUp(false);
		if(k== KeyEvent.VK_DOWN) player.setDown(false);
		if(k== KeyEvent.VK_LEFT) player.setLeft(false);
		if(k== KeyEvent.VK_SPACE) player.setJumping(false);
		if(k== KeyEvent.VK_E) player.setGliding(false);
		if(k== KeyEvent.VK_SHIFT) player.setBuff(false);
		if(k== KeyEvent.VK_CONTROL) player.setDebuff(false);
	}
}
